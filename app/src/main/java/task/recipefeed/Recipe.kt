package task.recipefeed

//modeling recipe feed structre

class RecipeFeed(val recipes: List<Recipe>)

class Recipe(val title: String, val description: String, val ingredients: Array<Ingredient>, val images: Array<RecipeImage>)

class Ingredient(val id: Int, val name: String, val elements: Array<Element>)

class RecipeImage(val imboid: String, val url: String)

class  Element(val id: Int, val amount: Float, val hint: String, val name: String, val unitName: String, val symbol: String, val menyCategory: MenyCategory)

class MenyCategory(val id: Int, val name: String)

