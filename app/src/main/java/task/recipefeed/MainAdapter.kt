package task.recipefeed

import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.recipe_row.view.*
import org.xml.sax.XMLReader

class MainAdapter(var feed: RecipeFeed): RecyclerView.Adapter<RecipeViewHolder>() {

    //number of items to display
    override fun getItemCount(): Int {
        return feed.recipes.size // returning number of all fetched recipes
    }

    //populating RecyclerView with layout views for every recipe
    override fun onCreateViewHolder(viewGroup: ViewGroup, int: Int): RecipeViewHolder {
        val layoutInflater = LayoutInflater.from(viewGroup.context)
        val recipeRow = layoutInflater.inflate(R.layout.recipe_row, viewGroup, false)
        return RecipeViewHolder(recipeRow)
    }

    //setting recipe data from feed to each view
    override fun onBindViewHolder(recipeViewHolder: RecipeViewHolder, index: Int) {

        val rTitle = feed.recipes.get(index).title
        val rDescription = feed.recipes.get(index).description
        val rIngredients = feed.recipes.get(index).ingredients
        var listOfIngriedients = ""


        for ((i, ingredient: Ingredient) in rIngredients.withIndex()) {
            if (ingredient.name != "") listOfIngriedients += ingredient.name
            if (ingredient.name != "" && i != rIngredients.size - 1) listOfIngriedients += ", "
        }
        if(listOfIngriedients.isBlank()) recipeViewHolder.view.ingridients_text.visibility = View.GONE
        else listOfIngriedients = "Ingridients: " + listOfIngriedients


        //setting images from url to imageView utlizing Picasso lib
        val rImgUrl = feed.recipes.get(index).images.get(0).url
        val thumbnailImageView =  recipeViewHolder.view.imageView
        Picasso.get().load(rImgUrl).into(thumbnailImageView)

        recipeViewHolder.view.title_text.text = rTitle
        recipeViewHolder.view.description_text.text = rDescription
        recipeViewHolder.view.ingridients_text.text = listOfIngriedients

        val tag : Html.TagHandler = HtmlTagHandler()

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            recipeViewHolder.view.description_text.text = Html.fromHtml(rDescription, Html.FROM_HTML_MODE_LEGACY, null, tag)
        } else {
            @Suppress("DEPRECATION")
            recipeViewHolder.view.title_text.text = Html.fromHtml(rDescription)
        }
    }
}

//ViewHolder Class
class RecipeViewHolder(val view: View): RecyclerView.ViewHolder(view)

class HtmlTagHandler : Html.TagHandler {
    override fun handleTag(opening: Boolean, tag: String?, output: Editable?, xmlReader: XMLReader?) {
        if (tag == "br ")
            output?.append("/n")
    }
}
