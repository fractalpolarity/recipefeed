package task.recipefeed

import android.content.Context
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.*
import java.io.IOException

class MainActivity : AppCompatActivity() {

    private val rPrefsStorageLocation = "recipesStorage"
    private val rPrefsData = "recipeList"
    private lateinit var sharedPreferences:SharedPreferences


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedPreferences =  getSharedPreferences(rPrefsStorageLocation, Context.MODE_PRIVATE)
        setContentView(R.layout.activity_main)

        buttonRefresh.setOnClickListener({
            refreshView()
            Toast.makeText(this, "Refreshing...", Toast.LENGTH_SHORT).show()
        })

        recyclerView_m.layoutManager = LinearLayoutManager(this)

        //if data from feed already on device, load from shared preferences, else fetch feed
        if(sharedPreferences.getString(rPrefsData, "") !=  "") loadFromPrefs()
         else fetchAndSaveJson()
    }

    //fetching json using okHttp, on success - storing to shared prefs and maping json to model data class
    fun fetchAndSaveJson() {
        println("Attempting to fetch JSON")
        val url = "https://www.godt.no/api/getRecipesListDetailed?tags=&size=thumbnailmedium&ratio=1&limit=50&from=0"
        val request = Request.Builder().url(url).build()
        val client = OkHttpClient()
        client.newCall(request).enqueue(object: Callback{
            override fun onFailure(call: Call?, e: IOException?) {
                println("Request failed")
                loadFromPrefs()
            }
            override fun onResponse(call: Call?, response: Response?) {
                val body = response?.body()?.string()
                println("Request Success")
                println(body)
                val sharedPreferences =  getSharedPreferences(rPrefsStorageLocation, Context.MODE_PRIVATE)
                sharedPreferences.edit().putString(rPrefsData, body).apply()
                val feedRecipe = recipeFeedFromString(body)
                val adapter = MainAdapter(feedRecipe)
                runOnUiThread {
                    recyclerView_m.visibility = View.VISIBLE
                    recyclerView_m.adapter = adapter
                    Toast.makeText(baseContext, "Data from Api", Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun loadFromPrefs() {
        val feedRecipe = recipeFeedFromString(sharedPreferences.getString(rPrefsData, ""))
        recyclerView_m.adapter = MainAdapter(feedRecipe)
        println("List From Prefs")
        recyclerView_m.visibility = View.VISIBLE
        Toast.makeText(baseContext, "Data From Prefs", Toast.LENGTH_LONG).show()
    }

    private fun refreshView() {
        recyclerView_m.visibility = View.INVISIBLE
        fetchAndSaveJson()
    }

    //fun for maping json string to model class utilizing Gson
    fun recipeFeedFromString(string:String?) : RecipeFeed{
        val gson = GsonBuilder().create()
        val recipesArray: Array<Recipe> = gson.fromJson(string, Array<Recipe>::class.java)
        val recipeFeed = RecipeFeed(recipesArray.toList())
        return recipeFeed
    }
}

